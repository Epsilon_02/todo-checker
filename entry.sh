#!/bin/bash
set -e

#######################
## SETUP GLOBAL VARS ##
#######################

# first test if we have all variables we need
[ -z "${CI_REPO}" ] && { echo "'CI_REPO' not set"; exit 1; } || true
[ -z "${CI_REPO_URL}" ] && [ -z "${CI_FORGE_URL}" ] && { echo "'CI_REPO_URL' or 'CI_FORGE_URL' not set"; exit 1; } || true
[ -z "${CI_FORGE_TYPE}" ] && { echo "'CI_FORGE_TYPE' not set"; exit 1; } || true

# optional variables
[ -z "${PLUGIN_REPOSITORY_TOKEN}" ] && { echo "'PLUGIN_REPOSITORY_TOKEN' not set - ignoring"; } || true

# check currently unsupported forges
[ "${CI_FORGE_TYPE}" != "gitea" ] && { echo "${CI_FORGE_TYPE} is currently not supported"; exit 1; } || true

# set defaults and sanitize
PLUGIN_DEBUG="$(echo "${PLUGIN_DEBUG}" | tr '[:upper:]' '[:lower:]')"
[ -z "${PLUGIN_PREFIX_REGEX}" ] && { PLUGIN_PREFIX_REGEX='(TODO|FIXME)'; } || true
[ -z "${PLUGIN_MAIN_REGEX}" ] && { PLUGIN_MAIN_REGEX='( |)(\(|\(#)(?P<ISSUE_NUMBER>\d+)(\))'; } || true
WORKDIR="."
RIPGREP_ARGUMENTS=()
ITEM_SETTINGS=-1
CURL_BASIC_ARGUMENTS=()

#############################
## declare generic helpers ##
#############################

function debug() {
    [ "${PLUGIN_DEBUG}" == "true" ] && echo "DEBUG: $*" 1>&2 || true
}

function extract_forge_url() {
    URL="${CI_FORGE_URL}"
    # if CI_FORGE_URL is not set calculate based on repo link and repo fullname
    [ -z "${CI_FORGE_URL}" ] &&  URL="${CI_REPO_URL%"${CI_REPO}"*}" || true
    echo "$URL"
}

function ignore_files_arg() {
  [ -f ".rgignore" ] && RIPGREP_ARGUMENTS+=(--ignore-file .rgignore) || true
  [ -f ".ignore" ] && RIPGREP_ARGUMENTS+=(--ignore-file .ignore) || true
  [ -f ".gitignore" ] && RIPGREP_ARGUMENTS+=(--ignore-file .gitignore) || true
}

#########################
## declare gitea impl. ##
#########################

function craft_api_url_gitea() {
    local limit=$1
    local page=$2
    # append to root url the api path
    API="$(extract_forge_url)/api/v1/repos/${CI_REPO}/issues?state=open&limit=${limit}&page=${page}"
    # replace '//' with '/'
    echo "$API" | sed 's/\/\//\//g'
}

function craft_curl_arguments_gitea() {
  CURL_BASIC_ARGUMENTS+=(--silent --request "GET" --header 'accept: application/json')
  [ -n "${PLUGIN_REPOSITORY_TOKEN}" ] && CURL_BASIC_ARGUMENTS+=(--header "Authorization: token ${PLUGIN_REPOSITORY_TOKEN}") || true
}

function get_max_item_settings_gitea() {
  curl "${CURL_BASIC_ARGUMENTS[@]}" \
  "$(extract_forge_url)/api/v1/settings/api" \
  | jq --raw-output ".max_response_items"
}

function get_all_open_issues_count_gitea() {
    curl "${CURL_BASIC_ARGUMENTS[@]}" \
    --output /dev/null \
    --write-out "%{header_json}" \
    "$(craft_api_url_"${CI_FORGE_TYPE}" '1' '1')" | jq '."x-total-count"[0] | tonumber'
}

function get_all_open_issues_gitea() {
    local PAGE_LIMIT
    PAGE_LIMIT="$(get_max_item_settings_"${CI_FORGE_TYPE}")"
    local TOTAL_COUNT
    TOTAL_COUNT="$(get_all_open_issues_count_gitea)"

    [[ "${TOTAL_COUNT}" =~ ^[0-9]+$ ]] || { echo "could not obtain TOTAL_COUNT"; exit 10; }
    [[ "${PAGE_LIMIT}" =~ ^[0-9]+$ ]] || { echo "could not obtain PAGE_LIMIT"; exit 10; }

    # calculate pages
    local PAGES=$((TOTAL_COUNT/PAGE_LIMIT))
    [ "$((TOTAL_COUNT%PAGE_LIMIT))" != "0" ] && PAGES=$((PAGES+1)) || true

    debug "start gitea get issue loop, total ${TOTAL_COUNT} issues via ${PAGES} pages"

    for((i=1;i<=PAGES;i++))
    do
        debug "GET $(craft_api_url_"${CI_FORGE_TYPE}" "${ITEM_SETTINGS}" "${i}")"

        curl "${CURL_BASIC_ARGUMENTS[@]}" \
            "$(craft_api_url_"${CI_FORGE_TYPE}" "${PAGE_LIMIT}" "${i}")" | jq --raw-output ".[].number"
    done
}

############################
## declare main functions ##
############################

function check_wrong_todos() {
  # note https://github.com/rust-lang/regex/issues/127 did prevent us from inverted regex groups ...
  mapfile -t WRONG_TODOS < <(rg \
      --hidden \
      --ignore-case \
      --with-filename \
      --line-number \
      --only-matching \
      --regexp "${PLUGIN_PREFIX_REGEX}.*" \
      "${WORKDIR}" | rg \
        --invert-match \
        --regexp "${PLUGIN_PREFIX_REGEX}${PLUGIN_MAIN_REGEX}") || true

  for TODO in  "${WRONG_TODOS[@]}"; do
      TODO_ISSUE_FILE="$(echo "${TODO}" | cut -d ':' -f1-2)"
      echo "${TODO_ISSUE_FILE} -> found wrong TODO pattern"
  done

  [ -n "${WRONG_TODOS[0]}" ] && exit 255 || true
  debug no wrong TODO pattern found
}

function check_todos() {
    debug start check if TODOs have valid index numbers

    craft_curl_arguments_"${CI_FORGE_TYPE}"

    mapfile -t ISSUES < <(get_all_open_issues_"${CI_FORGE_TYPE}")

    mapfile -t EXISTING_TODO_ISSUES < <(rg \
                              --hidden \
                              --ignore-case \
                              --with-filename \
                              --line-number \
                              --only-matching \
                              --regexp "${PLUGIN_PREFIX_REGEX}${PLUGIN_MAIN_REGEX}" \
                              --replace '$ISSUE_NUMBER' \
                              "${RIPGREP_ARGUMENTS[@]}" \
                              "${WORKDIR}" \
                          )
    debug "EXISTING_TODO_ISSUES: $(echo "${EXISTING_TODO_ISSUES[@]}" | cut -d ':' -f3 | tr ' ' ', ')"

    FAIL="false"
    for TODO_ISSUE in "${EXISTING_TODO_ISSUES[@]}"; do
        local TODO_ISSUE_EXIST="false"
        TODO_ISSUE_FILE="$(echo "${TODO_ISSUE}" | cut -d ':' -f1-2)"
        TODO_ISSUE_NUMBER="$(echo "${TODO_ISSUE}" | cut -d ':' -f3)"

        for ISSUE in "${ISSUES[@]}"; do
            debug "'$ISSUE' == '$TODO_ISSUE_NUMBER'"
            [ "$ISSUE" == "$TODO_ISSUE_NUMBER" ] && TODO_ISSUE_EXIST="true" && break || true
        done
        [ "${TODO_ISSUE_EXIST}" == "false" ] && {
            echo "${TODO_ISSUE_FILE} -> found issue '${TODO_ISSUE_NUMBER}' but no corresponding open one in ${CI_FORGE_TYPE}"
            FAIL=true
        } || true
    done

    debug FAIL is "${FAIL}"

    [ "${FAIL}" == "true" ] && exit 255 || true
}

#####################
## START FUNC EXEC ##
#####################

debug "workdir is '${WORKDIR}'"

# setup args
ignore_files_arg

# checks ...
check_wrong_todos
check_todos
