---
name: TODO-Checker
author: Epsilon_02
description: Plugin to check if TODOs has an open issue number to the project repository (currently only gitea/forgejo supported)
tags: [todo, testing]
containerImage: codeberg.org/epsilon_02/todo-checker
containerImageUrl: https://codeberg.org/Epsilon_02/-/packages/container/todo-checker/latest
url: https://codeberg.org/Epsilon_02/todo-checker
---

## Settings

| Settings Name             | Default           | Description
| --------------------------| ----------------- | --------------------------------------------
| `prefix_regex`            | `(TODO\|FIXME)`                           | prefix which should a comment start to check if there should exist a issue number
| `main_regex`              | `( \|)(\(\|\(#)(?P<ISSUE_NUMBER>\d+)(\))` | main regex to check how the issue number should be defined. IMPORTANT: if you want to build a custom regex, the name of the capturing group "ISSUE_NUMBER" have to be defined which only returns the issue-number!
| `repository_token`        | ``                                        | token if the repository to be checked is private
| `debug`                   | `false`                                   | enable debug output

### Building custom regex

This plugin uses [ripgrep](https://github.com/BurntSushi/ripgrep) to search through project files which uses the [rust regex syntax](https://docs.rs/regex/1.8.3/regex/#syntax).

### Ignore files
Ripgrep pays attention to the following files: `.rgignore`, `.ignore` and `.gitignore`.
It treats `.rgignore`-files with higher precedence than `.ignore`-files and `.ignore`-files with a higher precedence than `.gitignore.
Check also the [ripgrep manual](https://github.com/BurntSushi/ripgrep/blob/master/GUIDE.md#automatic-filtering) out for further reading.
